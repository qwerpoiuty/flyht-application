# flyht-application-front

## Project setup

Please start the backend before starting the front. As before, this application requires the LTS version of Node and at least version 5.2.0 of NPM.

This front is fully bundled and ready to be served. Just navigate to the root folder, run the command ```npm start```, and paste the address into your browser.

If you want to run the uncompiled version locally and make changes to the source, please run the command ```npm install``` to install dependencies followed by the command ```npm run serve```. Once you've made your changes, if you want to create a new bundle to serve, run the command ```npm run build``` to create the bundle. Once that completes, run the command ```npm start```.

Thank you for your time and consideration.
