import Vue from 'vue'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import './registerServiceWorker'
import locale from 'element-ui/lib/locale/lang/en'
Vue.use(Element, { locale })

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
