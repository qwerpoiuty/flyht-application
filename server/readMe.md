# flyht-application-back

## Project setup
 You'll need to download and install NodeJS and NPM before being able to run this application. Please follow the instructions at https://nodejs.org/en/download/. This application was written on version 10.16.0, the LTS version and requires version 5.2.0 of NPM or later.

 Once you've installed node and npm, open your terminal, navigate to this root folder and run command ```npm install``` to install the dependencies of the project. Once those dependencies have finished installing, run the command ```npm start``` to start the backend application. Please start this application before starting the front end application.