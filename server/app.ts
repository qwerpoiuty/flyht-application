import express, { NextFunction, Response, Request } from 'express';

import bodyParser from 'body-parser';
// import session from 'express-session'
// import passport from 'passport'
import util from 'util';
import chalk from 'chalk';

import { database } from './db';

import { Sequelize } from 'sequelize';
import cors from 'cors'
import Student from './db/models/students';

class App {
  public app: express.Application;
  public db: Sequelize;

  constructor() {
    this.db = database;
    this.app = express();
    this.config();
    this.db.sync();
  }

  private config(): void {
    // define a route handler for the default home page
    this.app.use(cors())
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    if (process.env.NODE_ENV === 'dev') {
      this.app.use((req: express.Request, res: express.Response, next: express.NextFunction): void => {
        util.log(('---NEW REQUEST---'));
        console.log(util.format(chalk.red('%s: %s %s'), 'REQUEST ', req.method, req.path));
        console.log(util.format(chalk.yellow('%s: %s'), 'QUERY   ', util.inspect(req.query)));
        console.log(util.format(chalk.cyan('%s: %s'), 'BODY    ', util.inspect(req.body)));
        next()
      });
    }
    // normally I would import the routes for the api here, however given the small size of this project, I don't feel the extra files are necessary

    this.app.get('/api/students', async (req: Request, res: Response, next: NextFunction) => {
      try {
        const students = await Student.findAll();
        res.status(200).json(students);
      } catch (err) {
        res.status(500).json({ msg: 'Internal Server Error' });
      }
    });

    this.app.post('/api/students', async (req: Request, res: Response, next: NextFunction) => {
      const { body } = req;
      try {
        const newStudent = await Student.create(body);
        res.status(200).json(newStudent);
      } catch (err) {
        res.status(500).json({ msg: `Internal Server Error` });
      }
    });

    this.app.put('/api/students/:studentId', async (req: Request, res: Response, next: NextFunction) => {
      const { body } = req;
      const { studentId } = req.params
      try {
        const updatedStudent = await Student.update(body, { where: { studentId }, returning: true });
        res.status(200).json(updatedStudent)
      } catch (err) {
        res.status(500).json({ msg: `Internal Server Error` })
      }
    });

  };

};

export default new App().app;
