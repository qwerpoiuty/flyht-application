import app from './app';
import chalk from 'chalk';
const port = process.env.SERVER_PORT || 2337;

app.listen(port, () => {
  console.log(chalk.red(`server started at http://localhost:${port}`));
});