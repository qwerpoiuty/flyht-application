import { Sequelize } from 'sequelize';
import Students from './models/students'

let database: Sequelize;
// here we will connect to the proper database depending on environment. also definitely store the credentials here as environmental variables in production. 
database = new Sequelize('postgres://eznrucku:G1ryTh8XPHnnIivZH0vn6xRBDAU4LVS9@otto.db.elephantsql.com:5432/eznrucku' as string, { 'logging': console.log });

// don't forget to add the imported models to the models array!
const models = [ // order of models might be important because of associations
  Students
];

models.forEach(model => {
  model.initialize(database);
});

models.forEach(model => {
  model.resolveAssociations();
});

export { database };
