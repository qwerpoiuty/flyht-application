import { Sequelize, Model, DataTypes, ModelAttributes } from 'sequelize';

class Student extends Model {
  public studentId?: number;
  public firstName?: string;
  public lastName?: string;
  public phoneNumber?: number;
  public status?: string;


  static attributes: ModelAttributes = {
    studentId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    phoneNumber: DataTypes.INTEGER,
    status: { type: DataTypes.ENUM('active', 'delinquent', 'dropped'), defaultValue: 'active' }
  };

  static initialize(sequelize: Sequelize): void {
    this.init(this.attributes, {
      sequelize,
      modelName: 'student',
      defaultScope: {
        attributes: {
          exclude: ['createdAt', 'updatedAt']
        }
      }
    });
  };

  static resolveAssociations(): void {
    //any associations we can plan to have will be here. Maybe grades, classes enrolled, etc.
  };

};

export default Student;
